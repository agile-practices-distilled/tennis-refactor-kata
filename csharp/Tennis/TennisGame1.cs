using System.Collections.Generic;

namespace Tennis
{
    class TennisGame1 : ITennisGame
    {
        private const int DEUCE_LIMIT = 2;
        private const int ADVANTAGE_PHASE_LIMIT = 3;
        
        private int _player1Score;
        private int _player2Score;
        private readonly string _player1Name;
        private readonly string _player2Name;
        private readonly Dictionary<int,string> _scoreMap = new Dictionary<int, string>
        {
            {0, "Love"},
            {1, "Fifteen"},
            {2, "Thirty"},
            {3, "Forty"},
        };

        public TennisGame1(string player1Name, string player2Name)
        {
            this._player1Name = player1Name;
            this._player2Name = player2Name;
        }

        public void WonPoint(string playerName)
        {
            if (playerName == _player1Name)
                _player1Score++;
            else
                _player2Score++;
        }

        public string GetScore()
        {
            if (GameIsDraw())
            {
                return GetDrawScore();
            }
            if (GameIsInAdvantagePhase())
            {
                return GetAdvantagePhaseScore();
            }

            return GetStandardScore();
        }

        private string GetStandardScore()
        {
            return $"{_scoreMap[_player1Score]}-{_scoreMap[_player2Score]}";
        }

        private string GetAdvantagePhaseScore()
        {
            var scoreDifference = _player1Score - _player2Score;

            if (scoreDifference == 1) 
                return GetAdvantageMessageFor(_player1Name);
            if (scoreDifference == -1) 
                return GetAdvantageMessageFor(_player2Name);
            
            if (scoreDifference >= 2) 
                return GetWinMessageFor(_player1Name);
            
            return GetWinMessageFor(_player2Name);
        }

        private static string GetAdvantageMessageFor(string playerName)
        {
            return $"Advantage {playerName}";
        }

        private static string GetWinMessageFor(string playerName)
        {
            return $"Win for {playerName}";
        }

        private string GetDrawScore()
        {
            return _player1Score > DEUCE_LIMIT ? "Deuce" : $"{_scoreMap[_player1Score]}-All";
        }

        private bool GameIsInAdvantagePhase()
        {
            return _player1Score > ADVANTAGE_PHASE_LIMIT || _player2Score > ADVANTAGE_PHASE_LIMIT;
        }

        private bool GameIsDraw()
        {
            return _player1Score == _player2Score;
        }
    }
}

